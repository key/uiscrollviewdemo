================
UIScrollViewDemo
================

This project is UIScrollView demo.
You can change scroll deceleration rate, bounds, scrollbar visibility.

.. image:: https://bitbucket.org/key/uiscrollviewdemo/raw/master/screenshot.png
