//
//  ViewController.h
//  UIScrollViewDemo
//
//  Created by Mitsukuni Sato on 3/3/13.
//  Copyright (c) 2013 MyBike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIScrollViewDelegate>

@end
