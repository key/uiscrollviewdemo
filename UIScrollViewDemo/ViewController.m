//
//  ViewController.m
//  UIScrollViewDemo
//
//  Created by Mitsukuni Sato on 3/3/13.
//  Copyright (c) 2013 MyBike. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UISwitch *bounceSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *bounceHorizontalSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *bounceVerticalSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *scrollbarVerticalSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *scrollbarHorizontalSwitch;
@property (strong, nonatomic) IBOutlet UILabel *decelerationRateLabel;
@property (strong, nonatomic) IBOutlet UISlider *decelerationRateSlider;

@property (nonatomic) BOOL bounce;
@property (nonatomic) BOOL bounceHorizontal;
@property (nonatomic) BOOL bounceVertical;
@property (nonatomic) BOOL scrollbarHorizontal;
@property (nonatomic) BOOL scrollbarVertical;
@property (nonatomic) float decelerationRate;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // scrollview
    [self.scrollView setContentSize:self.contentView.frame.size];

    self.bounce = self.scrollView.bounces;
    [self.bounceSwitch setOn:self.bounce];
    [self.bounceSwitch setSelected:self.bounce];

    [self.scrollView setDecelerationRate:UIScrollViewDecelerationRateNormal];
    self.decelerationRate = self.scrollView.decelerationRate;
    [self.decelerationRateLabel setText:[NSString stringWithFormat:@"%.3f", self.decelerationRate]];

    // slider
    [self.decelerationRateSlider setMinimumValue:0.0f];
    [self.decelerationRateSlider setMaximumValue:1.0f];
    [self.decelerationRateSlider setValue:self.decelerationRate];
}

- (void)viewWillAppear:(BOOL)animated
{
    // scrollbar
    self.scrollbarHorizontal = self.scrollView.showsHorizontalScrollIndicator;
    [self.scrollbarHorizontalSwitch setOn:self.scrollbarHorizontal];
    [self.scrollbarHorizontalSwitch setSelected:self.scrollbarHorizontal];

    self.scrollbarVertical = self.scrollView.showsVerticalScrollIndicator;
    [self.scrollbarVerticalSwitch setOn:self.scrollbarVertical];
    [self.scrollbarVerticalSwitch setSelected:self.scrollbarVertical];

    // scrollview
    // bounce
    self.bounceHorizontal = self.scrollView.alwaysBounceHorizontal;
    [self.bounceHorizontalSwitch setOn:self.bounceHorizontal];
    [self.bounceHorizontalSwitch setSelected:self.bounceHorizontal];

    self.bounceVertical = self.scrollView.alwaysBounceVertical;
    [self.bounceVerticalSwitch setOn:self.bounceVertical];
    [self.bounceVerticalSwitch setSelected:self.bounceVertical];

    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)bounceSwitchAction:(id)sender
{
    self.bounce = !self.bounce;
    [self.scrollView setBounces:self.bounce];
}

- (IBAction)bounceHorizontalAction:(id)sender
{
    self.bounceHorizontal = !self.bounceHorizontal;
    [self.scrollView setAlwaysBounceHorizontal:self.bounceHorizontal];
}

- (IBAction)bounceVerticalButton:(id)sender
{
    self.bounceVertical = !self.bounceVertical;
    [self.scrollView setAlwaysBounceVertical:self.bounceVertical];
}

- (IBAction)scrollbarHorizontalAction:(id)sender
{
    self.scrollbarHorizontal = !self.scrollbarHorizontal;
    [self.scrollView setShowsHorizontalScrollIndicator:self.scrollbarHorizontal];
}

- (IBAction)scrollbarVerticalAction:(id)sender
{
    self.scrollbarVertical = !self.scrollbarVertical;
    [self.scrollView setShowsVerticalScrollIndicator:self.scrollbarVertical];
}

- (IBAction)sliderValueChanged:(id)sender
{
    self.decelerationRate = self.decelerationRateSlider.value;
    [self.decelerationRateLabel setText:[NSString stringWithFormat:@"%.3f", self.decelerationRate]];
    [self.scrollView setDecelerationRate:self.decelerationRate];
}

@end
